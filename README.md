# TCD CS7CS3 ASE project (Sustainable City) front end

Project description: This is the front-end part of the TCD 21/22 ASE project. Whole project is written by React.js.

## -- Project structure:

- components: The public components which can be re-used in different scenarios.
- hooks: The customized hooks which can be used in the future(if required).
- network: The network functions which are applied to connecting with back-end service.
- pages: The different page components to be shown according to the route rules.
- store: The redux part to maintain the states.
- styles: The css files to render the different components.
- utils: The maths or useful functions to serve for the project.
- App.js: The main component in the project.
- index.js: The root file of the project.
- .gitlab-ci.yml: The CI/CD config file. (use the docker contains cypress and nodev.14 to run the install, build and test stage)
- cypress: e2e test for the front end.
    - integration: The test files for the e2e test

## -- To start the project:
### For npm:
- install dependencies: npm install
- start the project: npm run start
- test the project manually: npm run test
### For yarn:
- install dependencies: yarn
- start the project: yarn start
- test the project manually: yarn test

## -- Coding standard:

### Coding part:

1. Public component should be defined in the component folder (i.e., the sidebar can be used for all the components, so it is public). 
2. For the UI part, avoid writing own styles. Use the UI component provided by Material UI and the classes in bootstrap. Here is some examples for the classes: m-1, mt-1, mr-1, ml-1, mb-1, p-1, d-flex, justify-content-center, ... If some components must be written with own code, please use the flex-box, that's better for the responsitive layout.
3. If some components have the same logic, should create a parent component which contains the common part to avoid redundant.

### Test part:

1. Every time before pushing to the remote gitlab respository, should run the test cases locally with command **npm run test**, ensure all the test cases have passed.
2. The test cases should be defined clearly, and the similar ones should be defined with in one describe block. (i.e., see the sidebar test cases in the cypress folder)
3. Try to use the data-cy tag to identify the HTML element. Though the class or id also work, but the data-cy is best way to do that, which is also recommended in the official document.

## -- Notice

1. The commit message should be like: [task name]/[task description]. If writing the test cases or error handling, please use test and fix as the task name. (like the commit messages in the git history)

2. When writing the specific features, i.e., home page. Please first create a new branch with the specific feature name.

   ```shell
   git branch -a feat/homePage
   ```

   Then write the code in the new branch, push to the gitlab and make a pulling request to the develop branch. After the coding review, it will be merged. Not directly modify the develop and master branch.

## -- references to coding and test

- https://docs.cypress.io/guides/references/best-practices
- https://reactjs.org/docs/getting-started.html
- https://mui.com
- https://getbootstrap.com/docs/3.4/css/
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox
- https://learngitbranching.js.org/?locale=en_US



