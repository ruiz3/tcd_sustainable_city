export function getNearestStop(stops, locations) {
    let smallest_dis = Infinity
    let nearest_stop = null
    stops.forEach(stop => {
        let dis = (stop.lat - locations.lat) ** 2 + (stop.lng - locations.long) ** 2
        if (dis < smallest_dis) {
            smallest_dis = dis
            nearest_stop = stop
        }
    });
    return nearest_stop
}

export function getNearestNStops(stops, locations,n) {
    if(n>stops.length)
        return stops
    let smallest_dis = Infinity
    let nearest_stop = []
    stops.forEach(stop => {
        let dis = (stop.lat - locations.lat) ** 2 + (stop.lng - locations.long) ** 2
        if (dis < smallest_dis) {
            smallest_dis = dis
            nearest_stop.push(stop)
        }
    });
    return nearest_stop.slice(nearest_stop.length-n,nearest_stop.length)
}