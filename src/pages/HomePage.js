import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { get_smart_bike_action } from '../store/bike/actionCreators';
import { getIrishRailAction } from '../store/irishrail/actionCreators';
import { getLuasStopAction } from '../store/luas/actionCreators';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { getNearestNStops } from '../utils/getNearestStop';
import { Box, AppBar, Toolbar, Typography } from '@mui/material'
import CircularProgress from '@mui/material/CircularProgress';


function HomePage() {
    const [locations, setLocations] = useState({})
    const [stops, setStops] = useState([])
    const { bike_statis, irish_rail, luas } = useSelector((state) => ({
        bike_statis: state.bike.smart_bikes_statistics,
        irish_rail: state.irishrail.rails,
        luas: state.luas.stops
    }))
    useEffect(() => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(pos => {
                setLocations({
                    lat: pos.coords.latitude,
                    long: pos.coords.longitude
                })
            })

        } else {
            throw new Error('Location not allowed...')
        }
    }, [])
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(get_smart_bike_action())
        dispatch(getIrishRailAction())
        dispatch(getLuasStopAction())
    }, [dispatch])
    useEffect(() => {
        const res = Object.keys(locations).length ? getNearestNStops(luas, locations, 5) : []
        setStops(res)
    }, [locations, luas])
    return (
        Object.keys(bike_statis).length && irish_rail.length && luas.length && stops.length ?
            <div>
                <Box>
                    <AppBar position="static">
                        <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
                            <Typography variant="h6" color="inherit" component="div">
                                About Dublin
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </Box>
                <div className='row home-card mt-5 d-flex justify-content-around'>
                    <Card className='col-3'>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../styles/img/bike1.png')}
                            alt="bike"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Dublin Bikes
                            </Typography>
                            <Typography className='mt-3'>
                                Total Bikes: {bike_statis.total_bikes}
                            </Typography>
                            <Typography className='mt-3'>
                                Total Stations: {bike_statis.total_bike_stands}
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card className='col-3'>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../styles/img/irish_rail.jpeg')}
                            alt="rail"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Irish Rail
                            </Typography>
                            <div className='d-flex justify-content-between flex-wrap' style={{ listStyle: 'none' }}>
                                <div style={{ width: '33%' }}>Dep</div>
                                <div style={{ width: '33%' }}>Arr</div>
                                <div style={{ width: '33%' }}>Time</div>
                                <div style={{ width: '33%' }}>{irish_rail[0].destination_station}</div>
                                <div style={{ width: '33%' }}>{irish_rail[0].originating_station}</div>
                                <div style={{ width: '33%' }}>{irish_rail[0].departure_time}</div>
                                <div style={{ width: '33%' }}>{irish_rail[1].destination_station}</div>
                                <div style={{ width: '33%' }}>{irish_rail[1].originating_station}</div>
                                <div style={{ width: '33%' }}>{irish_rail[1].departure_time}</div>
                            </div>
                        </CardContent>
                    </Card>
                    <Card className='col-3'>
                        <CardMedia
                            component="img"
                            height="140"
                            image={require('../styles/img/quiet_luas.jpeg')}
                            alt="rail"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                Luas stop nearby
                            </Typography>
                            <div className='d-flex justify-content-between flex-wrap' style={{ listStyle: 'none' }}>
                                {stops.map(stop => (
                                    <div style={{ width: '100%' }}>{stop.stop_name}</div>
                                ))}
                            </div>
                        </CardContent>
                    </Card>
                </div>

                <div className='dublin-intro d-flex justify-content-around align-items-center mt-5'>
                    <div className='w-50'>
                        <h3 className='mt-5'>About Dublin</h3>
                        <div style={{ 'textAlign': 'justify' }}>
                            <p>Dublin, Irish Dubh Linn, Norse Dyfflin (“Black Pool”), also called Baile Átha Cliath (“Town of the Ford of the Hurdle”), city, capital of Ireland, located on the east coast in the province of Leinster. </p>
                            <p>Dublin is a warm and welcoming city, known for the friendliness of its people and famous for its craic (“crack”)—that mixture of repartee, humour, intelligence, and acerbic and deflating insight that has attracted writers, intellectuals, and visitors for centuries. It has faded grandeur and a comfortably worn sense. Some one-fourth of the residents of the Republic of Ireland live in the Greater Dublin urban area, providing a good deal of bustle. The city’s heart is divided north-south by the River Liffey, with O’Connell’s Bridge connecting the two parts. Pubs (where much of the city’s social life is conducted), cafés, and restaurants abound, and Irish musicality rarely allows silence. On the north side, near the General Post Office, stand most of the remaining Georgian houses, built in the 18th century around squares, now side by side with glass and concrete offices and apartment blocks.</p>
                        </div>
                    </div>
                    <img width={'33%'} src={require('../styles/img/dublin_map.png')} alt="map" />
                </div>

                <div className='home-content d-flex justify-content-around mt-5'>
                    <div className='w-50'>
                        <h3 className='mt-5'>Dublin Transport</h3>
                        <div style={{ 'textAlign': 'justify' }}>
                            <p>Luas: A light rail tram network called the Luas, named after the Irish word for "speed". The service began with two disconnected lines in 2004, with three extensions opened in 2009, 2010 and 2011 before a cross-city link between the lines and further extension opened in 2017.</p>
                            <p>Bus: Bus services in Dublin are operated for the most part by state owned Dublin Bus but a number of peripheral bus routes are provided by Go-Ahead Ireland a private operator who operate a number of peripheral bus routes on behalf of the NTA. There is an extensive bus network of 110 radial, cross-city and peripheral routes in the Greater Dublin Area, which constitutes the bulk of the area's public transport system. Daytime routes are identified by number and sometimes suffixed with a letter (e.g. 25, 25a, 25b) and 18 "Nitelink" overnight services which run on Friday and Saturday nights are identified by a number suffixed with "N" (e.g. 41n). There are two 24-hour services for which there is no N equivalent and daytime fares are charged all day.</p>
                            <p>DART: The Dublin Area Rapid Transit (DART) is part of the suburban railway network and consists of one line and a spur running primarily along the coastline of Dublin Bay, from Greystones in County Wicklow to Howth and Malahide in northern County Dublin. The DART line is the only electrified railway in the country and over 80,000 people use it every day making it arguably Ireland's greatest public transport success story.</p>
                            <p>Dublin Bikes: Dublin Bikes is a public bicycle rental scheme which has been operated in the city of Dublin since 2009. The scheme uses 1600 French-made unisex bicycles with a silver colour. </p>
                        </div>
                    </div>
                    <img width={'40%'} src={require('../styles/img/transportation.png')} alt="transport" />
                </div>
            </div> : 
        <div className = 'loading-container' style = {{display: 'flex',justifyContent:'center',alignItems:'center',height:'100vh'}}>
            <Box>
             <CircularProgress />
           </Box>
        </div>
    );
}

export default HomePage;
