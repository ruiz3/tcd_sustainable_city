import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getIrishRailAction } from '../store/irishrail/actionCreators';
import { Select, MenuItem } from '@mui/material';
import TrainTimeTable from '../components/trainTimeTable';
import RailTravelInfo from '../components/railTravelInfo';
import { Box, AppBar, Toolbar, Typography } from '@mui/material'

function TrainPage() {
    const { rails } = useSelector((state) => ({
        rails: state.irishrail.rails
    }))
    const [val, setVal] = useState()
    const [station, setStation] = useState('')
    const [selectedRails, setSelectedRails] = useState([])
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getIrishRailAction())
    }, [dispatch])
    useEffect(() => {
        if (station) {
            setSelectedRails(rails.filter(rail => {
                return rail.originating_station === station.originating_station
            }))
        }
        return () => {
            setSelectedRails([])
        }
    }, [station, rails])
    return (
        <div>
            <Box>
                <AppBar position="static">
                    <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
                        <Typography variant="h6" color="inherit" component="div">
                            Irish Rail
                        </Typography>
                    </Toolbar>
                </AppBar>
            </Box>
            <div className='border-end-0 border-info w-100 mt-3' style={{ padding: '0% 5%' }}>
                <div className='rail-header d-flex justify-content-between'>
                    <h3>Railway Time Table</h3>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={val}
                        label="train"
                        data-cy="select"
                        onChange={(event) => {
                            console.log(event.target.value)
                            setVal(event.target.value)
                            setStation(rails[event.target.value])
                        }}>
                        {rails.map((station, index) => (
                            <MenuItem key={station.train_code} value={index}>
                                {station.originating_station}
                            </MenuItem>
                        ))}
                    </Select>
                </div>
                <div className='mt-4 rail-timetable'>
                    <TrainTimeTable rails={selectedRails.length ? selectedRails : rails} />
                </div>
                <div className='mt-5 rail-travel-info'>
                    <RailTravelInfo />
                </div>
            </div>
        </div>
    );
}

export default TrainPage;
