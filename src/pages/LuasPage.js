import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { getLuasStopAction } from '../store/luas/actionCreators';
import { getNearestStop } from '../utils/getNearestStop'
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import LuasTimeTable from '../components/luasTimeTable';
import LuasStopMap from '../components/luasStopMap';
import { Box, AppBar, Toolbar, Typography } from '@mui/material';
import CircularProgress from '@mui/material/CircularProgress';

function LuasPage() {
    const [locations, setLocations] = useState({})
    const [stop, setStop] = useState({})
    const [val, setVal] = useState()
    const { stops } = useSelector(state => ({
        stops: state.luas.stops,
    }), shallowEqual)
    const dispatch = useDispatch()
    useEffect(() => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(pos => {
                setLocations({
                    lat: pos.coords.latitude,
                    long: pos.coords.longitude
                })
            })
        } else {
            throw new Error('Location not allowed...')
        }
    }, [])
    useEffect(() => {
        dispatch(getLuasStopAction())
    }, [dispatch])
    useEffect(() => {
        stops.length && Object.keys(locations).length && setStop(getNearestStop(stops, locations))
        return () => {
            setStop({})
        }
    }, [stops, locations])
    return (
        <div>
            {Object.keys(stop).length ? (
                <>
                    <Box>
                        <AppBar position="static">
                            <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
                                <Typography variant="h6" color="inherit" component="div">
                                    Luas
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </Box>
                    <div className='container border border-success w-100 border-bottom-0'>
                        <div className='d-flex justify-content-between'>
                            <h2 className='text-primary mt-3'>{stop.stop_name}</h2>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={val}
                                label="stop"
                                data-cy="select"
                                onChange={(event) => {
                                    setVal(event.target.value)
                                    setStop(stops[event.target.value])
                                }}
                            >
                                {stops.map((stop, index) => (
                                    <MenuItem key={stop.abbr} value={index}>
                                        {stop.stop_name}
                                    </MenuItem>
                                ))}
                            </Select>
                        </div>
                        {/* time table */}
                        <div className='d-flex justify-content-center'>
                            <LuasTimeTable stop={stop} inbound={true} />
                        </div>
                        <div className='d-flex justify-content-center'>
                            <LuasTimeTable stop={stop} inbound={false} />
                        </div>
                        {/* Operating Hours */}
                        <div className='operating-hours mt-5'>
                            <h3>Operating Hours</h3>
                            <div className='d-flex justify-content-around mt-3'>
                                <table className='table w-25 table-bordered'>
                                    <thead className='table-primary'>
                                        <tr>
                                            <th colSpan={2} scope="col">Monday-to-Friday</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>First Tram</td>
                                            <td>05:52</td>
                                        </tr>
                                        <tr>
                                            <td>Last Tram</td>
                                            <td>00:38</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table className='table w-25 table-bordered'>
                                    <thead className='table-primary'>
                                        <tr>
                                            <th colSpan={2} scope="col">Saturday</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>First Tram</td>
                                            <td>06:52</td>
                                        </tr>
                                        <tr>
                                            <td>Last Tram</td>
                                            <td>00:38</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table className='table w-25 table-bordered'>
                                    <thead className='table-primary'>
                                        <tr>
                                            <th colSpan={2} scope="col">Sunday or Bank Holidays</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>First Tram</td>
                                            <td>07:22</td>
                                        </tr>
                                        <tr>
                                            <td>Last Tram</td>
                                            <td>23:39</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <h3>Current stop location</h3>
                        <LuasStopMap stop={stop} />
                        <div className="line-break" style={{ width: '100%',height:'10vh' }}></div>
                        {/* travel update */}
                        <div className='mt-3'>
                            <h3>Travel Update</h3>
                            <div className='border-top'></div>
                            <div className='mt-3'>Red Line services operating normally</div>
                            <div className='mt-3'>Green Line services operating normally</div>
                            <ul className='mt-3'>
                                <li>Website: <a href='www.luas.ie' style={{ textDecoration: 'none' }}>Luas.ie</a></li>
                                <li>Email: info@luas.ie</li>
                            </ul>
                        </div>
                        {/* map */}
                    </div>
                </>
            ) : 
            <div className = 'loading-container' style = {{display: 'flex',justifyContent:'center',alignItems:'center',height:'100vh'}}>
                <Box>
                <CircularProgress />
                </Box>
            </div>}
        </div>
    );
}

export default LuasPage;
