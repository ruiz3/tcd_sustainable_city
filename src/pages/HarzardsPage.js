import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import 'mapbox-gl/dist/mapbox-gl.css';
import roadClosed from '../styles/img/road_closed.png';
import roadJam from '../styles/img/traffic_jam.png'
import accidentMarker from '../styles/img/accident_marker.png'
import { get_hazard_action } from '../store/Hazards/actionCreators'
import ReactMapGL, { Marker, Popup } from 'react-map-gl';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';



const TOKEN = 'pk.eyJ1Ijoic2Rhc2giLCJhIjoiY2wxYzZhcGd1MDNydjNndGY5bWg3bG5yMSJ9.3bbjCSSNs9pTNsOFyY1FLQ';

// Viewport settings
const INITIAL_VIEW_STATE = {
  latitude: 53.350140,
  longitude: -6.266155,
  zoom: 14,
  pitch: 30,
  bearing: 0
};

function HarzardsPage() {
  const [viewport, setViewport] = useState()

  const { incidents } = useSelector(state => ({ incidents: state.hazard.incidents }), shallowEqual)


  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(get_hazard_action())
  }, [dispatch])

  const [selectedMarker, setSelectedMarker] = useState(null);
  const [selectedMarkerType, setSelectedMarkerType] = useState(null);

  return (
    <div style={{ height: '100vh' }}>
      <Box>
        <AppBar position="static">
          <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
            <Typography variant="h6" color="inherit" component="div">
              Real Time Traffic Information
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>
      <div style={{ height: '80vh', width: '90%', margin: '3% 5%' }}>
        <ReactMapGL
          initialViewState={INITIAL_VIEW_STATE}
          onViewportChange={(new_view) => setViewport(new_view)}
          mapStyle="mapbox://styles/mapbox/dark-v10"
          mapboxAccessToken={TOKEN}

        >

          {
            Object.keys(incidents).length && incidents['Road Closed'].map((d, index) => (<Marker
              key={`Marker-1-${index}`}
              latitude={Number(d[1])}
              longitude={Number(d[0])}>
              <img src={roadClosed} style={{ height: '30px', width: '30px' }} onClick={(e) => {
                e.preventDefault()
                setSelectedMarker(d)
                setSelectedMarkerType('Road Closed')
              }} alt="closed" />
            </Marker>))
          }
          {
            Object.keys(incidents).length && incidents['Jam'].map((d, index) => (<Marker
              key={`Marker-2-${index}`}
              latitude={Number(d[1])}
              longitude={Number(d[0])}>
              <img src={roadJam} style={{ height: '30px', width: '30px' }} onClick={(e) => {
                e.preventDefault()
                setSelectedMarker(d)
                setSelectedMarkerType('Traffic Jam')
              }} alt="jam" />
            </Marker>))
          }
          {
            Object.keys(incidents).length && incidents['Accident'].map((d, index) => (<Marker
              key={`Marker-3-${index}`}
              latitude={Number(d[1])}
              longitude={Number(d[0])}>
              <img src={accidentMarker} style={{ height: '30px', width: '30px' }} onClick={(e) => {
                e.preventDefault()
                setSelectedMarker(d)
                setSelectedMarkerType('Accident')
              }} alt="accident" />
            </Marker>))
          }
          {selectedMarker ? (
            <Popup latitude={selectedMarker[1]} longitude={selectedMarker[0]} onClose={() => {
              setSelectedMarker(null)
              setSelectedMarkerType(null)
            }}>
              <div style={{ backgroundColor: 'Black', color: 'White', padding: '5px' }} >
                {selectedMarkerType}
              </div>
            </Popup>
          ) : null}
        </ReactMapGL>
      </div>
    </div>
  );
}

export default HarzardsPage;
