import React from 'react';
import IncidentForm from '../components/incidentForm';
import { Box, AppBar, Toolbar, Typography } from '@mui/material'

function IncidentPage({ user }) {
    return (
        <>
            <Box>
                <AppBar position="static">
                    <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
                        <Typography variant="h6" color="inherit" component="div">
                            Incident Report
                        </Typography>
                    </Toolbar>
                </AppBar>
            </Box>
            <div className='d-flex justify-content-center align-items-center w-100' style={{ height: '100vh' }}>
                <IncidentForm user={user} />
            </div>
        </>
    );
}

export default IncidentPage;
