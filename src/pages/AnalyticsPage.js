import React, { useEffect, useState } from 'react';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { getLuasStopAction } from '../store/luas/actionCreators';
import { getNearestStop, getNearestNStops } from '../utils/getNearestStop';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import MediaCard from '../components/card/Card';
import busyStation from '../styles/img/busy_station.jpeg';
import quietStation from '../styles/img/quiet_station.jpeg';
import busyLuas from '../styles/img/busy_luas.jpeg';
import quietLuas from '../styles/img/quiet_luas.jpeg';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import PieChart from '../components/pieChart';
import { getAnalyticsData } from '../network/analytics'

function AnalyticsPage() {
    // const bikesStationData = {
    //     busy_stations: [
    //         {
    //             address: "Blessington Street",
    //             latitude: "53.3568",
    //             longitude: "-6.26814",
    //             name: "BLESSINGTON STREET",
    //             station_id: 2
    //         },
    //         {
    //             address: "Dame Street",
    //             latitude: "53.344",
    //             longitude: "-6.2668",
    //             name: "DAME STREET",
    //             station_id: 10
    //         },
    //         {
    //             address: "St. Stephen's Green South",
    //             latitude: "53.3375",
    //             longitude: "-6.26199",
    //             name: "ST. STEPHEN'S GREEN SOUTH",
    //             station_id: 37
    //         }
    //     ],
    //     quiet_stations: [
    //         {
    //             address: "Parnell Street",
    //             name: "PARNELL STREET",
    //             latitude: "53.3509",
    //             longitude: "-6.26512",
    //             station_id: 88
    //         },
    //         {
    //             address: "Grangegorman Lower (South)",
    //             name: "GRANGEGORMAN LOWER (SOUTH)",
    //             latitude: "53.3547",
    //             longitude: "-6.27868",
    //             station_id: 99
    //         }
    //     ]
    // }

    const [busyLuasStations, setBusyLuasStations] = useState([])
    const [quietLuasStations, setQuietLuasStations] = useState([])
    const [busyBikeStations, setBusyBikeStations] = useState([])
    const [quietBikeStations, setQuietBikeStations] = useState([])
    const [getBikesTo, setGetBikesTo] = useState('')
    const [getBikesFrom, setGetBikesFrom] = useState([])
    const [bikesStationData, setBikesStationData] = useState({})
    const today = new Date()
    const tomorrow = new Date(today)
    tomorrow.setDate(tomorrow.getDate() + 1)

    const { stops } = useSelector(state => ({
        stops: state.luas.stops,
    }), shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getLuasStopAction())
    }, [dispatch])

    useEffect(() => {
        getAnalyticsData().then(resp => {
            setBikesStationData(resp)
            var LuasStop = []
            if (Object.keys(bikesStationData).length) {
                var BikeStation = JSON.parse(bikesStationData['will_need_bikes']).map(bikeStation => {
                    var nearestStop = getNearestStop(stops, {
                        lat: bikeStation.latitude,
                        long: bikeStation.longitude
                    })
                    if (nearestStop && !LuasStop.includes(nearestStop.stop_name))
                        LuasStop.push(nearestStop.stop_name)
                    return bikeStation.address
                })
                setBusyLuasStations(LuasStop)
                setBusyBikeStations(BikeStation)
                LuasStop = []
                var BikeStation = JSON.parse(bikesStationData['can_give_bikes']).map(bikeStation => {
                    var nearestStop = getNearestStop(stops, {
                        lat: bikeStation.latitude,
                        long: bikeStation.longitude
                    })
                    // BikeStation.push(bikeStation.name)
                    if (nearestStop && !LuasStop.includes(nearestStop.stop_name))
                        LuasStop.push(nearestStop.stop_name)
                    return bikeStation.address
                })
                setQuietLuasStations(LuasStop)
                setQuietBikeStations(BikeStation)
            }
        })
    }, [stops, bikesStationData])

    return (
        <div style={{ height: '100vh' }}>
            <Box>
                <AppBar position="static">
                    <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
                        <Typography variant="h6" color="inherit" component="div">
                            Analytics
                        </Typography>
                    </Toolbar>
                </AppBar>
            </Box>
            <h2 style={{ marginLeft: '5%', marginTop: '1%' }}>Predictions for {tomorrow.toDateString()}</h2>
            <Box
                sx={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-around',
                    width: '90%',
                    height: '80%',
                    marginTop: '1%',
                    marginLeft: '5%',
                    borderRadius: 2,
                    p: 2, border: '1px dashed grey',
                    overflow: 'scroll'
                }}
            >
                <MediaCard key='3' heading='Busy Bike Stations' body={busyBikeStations} displayPic={busyStation} height={320} />
                <MediaCard key='4' heading='Quiet Bike Stations' body={quietBikeStations} displayPic={quietStation} height={320} />
                <div className="line-break" style={{ width: '100%' }}></div>
                <PieChart data={bikesStationData} />
                <div className="line-break" style={{ width: '100%' }}></div>
                <Box
                    // sx={{
                    //     width: '100%',
                    //     height: 160,
                    //     margin: '0% 10%',
                    //     borderRadius:2,
                    //     p: 1, border: '3px dashed grey',
                    // }}
                    sx={{
                        display: "flex",
                        width: '100%',
                        height: 160,
                        margin: '0% 10%',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 2,
                        p: 1, border: '3px dashed grey',
                    }}
                >
                    You may move some bikes to
                    <Select
                        labelId="busy-bike-station"
                        id="busy-bike-station"
                        value={getBikesTo}
                        label="Busy Bike Station"
                        sx={{ margin: '0px 8px' }}
                        onChange={(event) => {
                            setGetBikesTo(event.target.value)
                            var stationObj = bikesStationData['will need bikes'].filter(station => station.address === busyBikeStations[event.target.value])
                            setGetBikesFrom(getNearestNStops(bikesStationData['can give bikes'], {
                                lat: stationObj[0].latitude,
                                long: stationObj[0].longitude
                            }, 3))
                        }}
                    >
                        {busyBikeStations.map((stop, index) => (
                            <MenuItem key={stop.abbr} value={index}>
                                {stop}
                            </MenuItem>
                        ))}
                    </Select>
                    from &nbsp; {getBikesFrom.map(bfStation => (<span> {bfStation.address} / </span>))}
                </Box>
                <div className="line-break" style={{ width: '100%', height:'10%' }}></div>
                <MediaCard key='1' heading='Busy Luas Stops' body={busyLuasStations} displayPic={busyLuas} height={320} />
                <MediaCard key='2' heading='Quiet Luas Stops' body={quietLuasStations} displayPic={quietLuas} height={320} />
            </Box>
        </div>
    );
}

export default AnalyticsPage;
