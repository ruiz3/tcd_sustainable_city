import React, { useEffect, useState } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import { getEvents } from '../network/event';



function EventsPage() {
  const [eventList, setEventList] = useState([])
  // const myEventsList = [{
  //   'title': 'All Day Event very long title',
  //   'allDay': true,
  //   'start': new Date(2022, 4, -23),
  //   'end': new Date(2022, 4, -22)
  // },
  // {
  //   'title': 'Long Event',
  //   'start': new Date(2022, 4, -20),
  //   'end': new Date(2022, 4, -19)
  // },

  // {
  //   'title': 'DTS STARTS',
  //   'start': new Date(2022, 4, -16),
  //   'end': new Date(2022, 4, -15)
  // },

  // {
  //   'title': 'DTS ENDS',
  //   'start': new Date(2022, 4, -12),
  //   'end': new Date(2022, 4, -11)
  // }]
  useEffect(() => {
    getEvents().then(res => {
      const event = res.events.map(item => {
        let temp = Object.assign({}, item)
        temp.start = new Date(`${temp.start} ${temp.time}`)
        temp.end = new Date(`${temp.end} ${temp.time}`)
        delete temp.time
        delete temp.location

        return temp
      })
      setEventList(event)
    })
  }, [])
  const localizer = momentLocalizer(moment)
  return (
    <>
      <Box>
        <AppBar position="static">
          <Toolbar variant="dense" sx={{ height: 64, backgroundColor: '#808080' }}>
            <Typography variant="h6" color="inherit" component="div">
              Upcoming Events
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>
      <div style={{ height: '100vh', marginTop: '5%' }} data-cy="calendar">
        <Calendar
          localizer={localizer}
          events={eventList}
          startAccessor="start"
          endAccessor="end"
          style={{ height: 500 }}
        />
      </div>
    </>
  );
}

export default EventsPage;
