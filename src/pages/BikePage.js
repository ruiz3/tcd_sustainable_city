import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import SearchBar from '../components/SearchBar';
import { get_smart_bike_action } from '../store/bike/actionCreators'
import BikeHeader from '../components/bikeHeader/BikeHeader'
import StationDetail from '../components/stationDetail/stationDetail';
import BikeDiagram from '../components/bikeDiagram/BikeDiagram';
import { subscribeToTopic } from '../network/iot';

function BikePage() {
    const [isShow, setIsShow] = useState(false)
    const [searchText, setSearchText] = useState('')
    const { smart_bikes, smart_bikes_statistics, predictions } = useSelector(state => ({
        smart_bikes: state.bike.smart_bikes,
        smart_bikes_statistics: state.bike.smart_bikes_statistics,
        predictions: state.bike.prediction_data
    }))
    console.log('smart_bikes:', smart_bikes)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(get_smart_bike_action())
        console.log('aaaaaaaaaaaaaa')
        subscribeToTopic('feed', dispatch)
    }, [dispatch])

    return (
        <div>
            <SearchBar label='Dublin Bikes' setIsShow={setIsShow} isShow={isShow} setSearchText={setSearchText} />
            <div className='container'>
                <BikeHeader statistics={smart_bikes_statistics} />
                {
                    isShow ? (
                        <StationDetail keyword={searchText} predictions={predictions} />
                    ) : (
                        <></>
                    )
                }
                <BikeDiagram bikes={smart_bikes} />
            </div>
        </div>
    );
}

export default BikePage;
