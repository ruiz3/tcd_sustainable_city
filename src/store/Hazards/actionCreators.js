import { CHANGE_INCIDENTS} from './constants'
import { getHazardsData } from '../../network/hazards'


const change_hazard_action = (data) => ({
    type: CHANGE_INCIDENTS,
    incidents: data
})


const get_hazard_action = () => {
    return dispatch => {
        getHazardsData().then(res => {
            dispatch(change_hazard_action(res))
        }).catch(error => {
            console.log(error)
        })
    }
}

export {get_hazard_action}