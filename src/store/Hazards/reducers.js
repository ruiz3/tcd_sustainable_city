import { CHANGE_INCIDENTS } from "./constants"

const initial_state = {
    incidents:{},   
}

const hazard_reducers = (state = initial_state, action) => {
    switch (action.type) {
        case CHANGE_INCIDENTS:
            return { ...state, incidents: action.incidents }
        default:
            return state
    }
}

export default hazard_reducers