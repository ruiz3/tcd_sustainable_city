import { CHANGE_RAILS_ACTION } from './constants'
const initial_state = {
    rails: []
}

const reducer = (state = initial_state, action) => {
    switch (action.type) {
        case CHANGE_RAILS_ACTION:
            return { ...state, rails: action.payload }
        default:
            return state
    }
}

export default reducer