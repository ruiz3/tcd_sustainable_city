import { getIrishRail } from "../../network/irishrail"
import { CHANGE_RAILS_ACTION } from "./constants"

export const changeIrishRailAction = (res) => ({
    type: CHANGE_RAILS_ACTION,
    payload: res
})

export const getIrishRailAction = () => {
    return dispatch => {
        getIrishRail().then(res => {
            dispatch(changeIrishRailAction(res))
        })
    }
}