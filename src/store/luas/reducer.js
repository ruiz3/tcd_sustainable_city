import { CHANGE_LUAS_STOPS } from './constants'

const initial_state = {
    stops: [],
}

const luas_reducer = (state = initial_state, action) => {
    switch (action.type) {
        case CHANGE_LUAS_STOPS:
            return { ...state, stops: action.payload }
        default:
            return state
    }
}
export default luas_reducer