import { getLuasStop } from "../../network/luas"
import { CHANGE_LUAS_STOPS } from "./constants"

export const changeLuasStopAction = (data) => ({
    type: CHANGE_LUAS_STOPS,
    payload: data
})

export const getLuasStopAction = () => {
    return dispatch => {
        getLuasStop().then(res => {
            dispatch(changeLuasStopAction(res))
        })
    }
}