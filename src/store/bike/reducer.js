import { CHANGE_PREDICTION_DATA, CHANGE_SMART_BIKE_DATA } from "./constants"

const initial_state = {
    smart_bikes_statistics: {},
    smart_bikes: [],
    prediction_data: {}
}

const bike_reducers = (state = initial_state, action) => {
    switch (action.type) {
        case CHANGE_SMART_BIKE_DATA:
            return { ...state, smart_bikes: action.smart_bikes, smart_bikes_statistics: action.smart_bikes_statistics }
        case CHANGE_PREDICTION_DATA:
            return { ...state, prediction_data: action.prediction_data }
        default:
            return state
    }
}

export default bike_reducers