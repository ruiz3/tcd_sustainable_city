import { getBikeData } from '../../network/bike'
import { CHANGE_SMART_BIKE_DATA, CHANGE_PREDICTION_DATA } from './constants'

const change_bike_action = (data) => ({
    type: CHANGE_SMART_BIKE_DATA,
    smart_bikes: data.slice(1, data.length),
    smart_bikes_statistics: data[0]
})

const change_prediction_data = (data) => ({
    type: CHANGE_PREDICTION_DATA,
    prediction_data: data
})

const get_smart_bike_action = () => {
    return dispatch => {
        getBikeData().then(res => {
            let temp = []
            res.slice(1, res.length).forEach(item => {
                temp.push({
                    name: item.name,
                    num: [item.available_bikes]
                })
            })
            if (!localStorage.getItem('avail_bikes')) {
                localStorage.setItem('avail_bikes', JSON.stringify(temp))
            }
            dispatch(change_bike_action(res))
        })
    }
}

const get_live_bike_action = (data) => {
    // store the data into localStorage
    let temp = []
    console.log(data)
    data.live.slice(1, data.live.length).forEach(item => {
        // const station = JSON.parse(localStorage.getItem('avail_bikes')).filter(i => i[0].name ? i[0].name == item.name : false)
        const station = JSON.parse(localStorage.getItem('avail_bikes')).filter(i => i.name == item.name)
        station[0].num.push(item.available_bikes)
        temp.push(station[0])
    })
    localStorage.setItem('avail_bikes', JSON.stringify(temp))
    return dispatch => {
        dispatch(change_bike_action(data.live))
    }
}

const get_prediction_bike_data = (data) => {
    console.log(data)
    return dispatch => {
        dispatch(change_prediction_data(data))
    }
}

export { get_smart_bike_action, get_live_bike_action, get_prediction_bike_data }