import { combineReducers } from "redux";
import bike_reducers from "./bike/reducer";
import luas_reducers from './luas/reducer'
import hazard_reducers from "./Hazards/reducers";
import irish_rail_reducers from './irishrail/reducer';

const combined_reducers = combineReducers({
    bike: bike_reducers,
    luas: luas_reducers,
    hazard: hazard_reducers,
    irishrail: irish_rail_reducers,
})

export default combined_reducers