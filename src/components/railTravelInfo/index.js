import React from 'react'
import DirectionsBikeIcon from '@mui/icons-material/DirectionsBike';
import Typography from '@mui/material/Typography';
import NetworkWifiIcon from '@mui/icons-material/NetworkWifi';
import AccessibleIcon from '@mui/icons-material/Accessible';
import PetsIcon from '@mui/icons-material/Pets';

function RailTravelInfo() {
    return (
        <>
            <h3>Travel Information</h3>
            <div className='rail-bike-info d-flex justify-content-around mt-5'>
                <DirectionsBikeIcon sx={{ fontSize: '8rem' }} />
                <div>
                    <h4>Bicycle</h4>
                    <Typography align='justify'>
                        Bicycle racks where provided, are free for customers who wish to avail of them.

                        Alternatively, bicycle lockers are also available to rent at some stations. Two companies operate bike lockers within some of our stations. For further information, see Bike Locker and Cyc-Lok. Go to Station Information section, choose your station and under ''Parking and Transport links'' you will be able to find what bicycle parking facilities are available.
                    </Typography>
                </div>
            </div>
            <div className='rail-bike-info d-flex justify-content-around mt-5'>
                <NetworkWifiIcon sx={{ fontSize: '8rem' }} />
                <div>
                    <h4>WIFI</h4>
                    <Typography align='justify'>
                        Simply turn on your laptop, connect to 'Irish Rail - WiFi' and open your internet browser.

                        Customers who encounter any problems with any Evad wifi related issues please get in contact with wifisupport@evad.ie or the helpline on 0818-293111<br />
                        --- Free Access Fast Connection
                    </Typography>
                </div>
            </div>
            <div className='rail-bike-info d-flex justify-content-around mt-5'>
                <AccessibleIcon sx={{ fontSize: '8rem' }} />
                <div>
                    <h4>Disabilities</h4>
                    <Typography align='justify'>
                        We endeavour to assist all customers with disabilities to travel on our rail services. With this in mind we ensure that our stations, trains and our information services are accessible to as many people as possible.<br />
                        For more: <a style={{ textDecoration: 'none' }} href='https://www.irishrail.ie/en-ie/Travel-Information/accessibility-onboard-trains'>Disabilities service</a>
                    </Typography>
                </div>
            </div>
            <div className='rail-bike-info d-flex justify-content-around mt-5'>
                <PetsIcon sx={{ fontSize: '8rem' }} />
                <div>
                    <h4>Pets</h4>
                    <Typography align='justify'>
                        <ul>
                            <li>Small dogs can be carried free of charge provided they travel on the owners lap</li>
                            <li>The dog must be kept on a lead at all times unless contained in a basket or in an appropriate container. Dogs on the restricted breeds list must be muzzled while travelling onboard our services</li>
                            <li>Small cats can be carried free of charge provided they travel in a secure carrier on the customer's lap</li>
                            <li>Animals are not allowed in restaurant cars with the exception of Guide dogs or Assistance dogs</li>
                            <li>If a customer objects to the presence of a dog in their carriage, the owner will be requested to move to another part of the train</li>
                        </ul>
                    </Typography>
                </div>
            </div>
        </>
    )
}

export default RailTravelInfo