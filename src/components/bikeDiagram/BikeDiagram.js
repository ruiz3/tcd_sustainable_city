import React, { useEffect, useState } from 'react'
import Plot from 'react-plotly.js';

function BikeDiagram({ bikes }) {
    const [bikeNames, setBikeNames] = useState([])
    const [bikeNums, setBikeNums] = useState([])
    useEffect(() => {
        // data processing
        const bike_names = []
        const bike_nums = []
        bikes.slice(0, 30).forEach(station => {
            bike_names.push(station.name.split(" ")[0])
            bike_nums.push(station.available_bikes)
        });
        console.log(bike_names)
        setBikeNames(bike_names)
        setBikeNums(bike_nums)
    }, [bikes])
    return (
        <Plot
            data={[
                {
                    x: bikeNums,
                    y: bikeNames,
                    marker: { color: '#0071a1' },
                    type: 'bar',
                    orientation: 'h'
                },
            ]}
            layout={{ title: 'Number of Bikes per Station', paper_bgcolor:'rgba(0,0,0,0)', plot_bgcolor:'rgba(0,0,0,0)',margin: {
                l: 150,
                r: 150,
                pad: 10
              } }}
            style={{height: 800}}
        />
    )
}

export default BikeDiagram