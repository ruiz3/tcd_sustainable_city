import React, { useEffect, useState } from 'react'
import Box from '@mui/material/Box';
import { FormControl, InputLabel, Input, MenuItem, Select, TextField, IconButton, Button } from '@mui/material';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import SendIcon from '@mui/icons-material/Send';
import { postIncident } from '../../network/incident';

function IncidentForm({ user }) {
    const [incidentType, setIncidentType] = useState(1)
    const [title, setTitle] = useState('Accident')
    const [image, setImage] = useState({})
    const [email, setEmail] = useState('')
    const [locations, setLocations] = useState({
        lat: 0,
        long: 0,
    })
    useEffect(() => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(pos => {
                setLocations({
                    lat: pos.coords.latitude,
                    long: pos.coords.longitude
                })
            })
        } else {
            throw new Error('Location not allowed...')
        }
    }, [locations])

    const handleSubmit = () => {
        const formData = new FormData()
        formData.append('image', image)
        formData.append('incidentType', incidentType)
        formData.append('title', title)
        formData.append('latitude', locations.lat)
        formData.append('longitude', locations.long)
        formData.append('email', email)
        formData.append('username', user.username)
        postIncident(formData, { "Content-Type": "multipart/form-data" }).then(res => {
            console.log(res)
            alert('Form Submitted Successfully')
            setIncidentType('')
            setEmail('')
            setTitle('')
        })
    }
    return (
        <div className='border border-dark p-5 rounded'>
            <h2>Incident Report</h2>
            <Box sx={{ minWidth: 120 }}>
                <div>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">incident type</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="incident type"
                            value={incidentType}
                            onChange={(e) => setIncidentType(e.target.value)}
                        >
                            <MenuItem value={1}>Property Damage</MenuItem>
                            <MenuItem value={2}>Medical Issue</MenuItem>
                            <MenuItem value={3}>Transportation Accident</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <div className='mt-3'>
                    <FormControl fullWidth>
                        <TextField
                            id="outlined-basic"
                            label="Email"
                            variant="outlined"
                            required
                            data-cy="email"
                            value={email}
                            onChange={(e) => {
                                setEmail(e.target.value)
                            }} />
                    </FormControl>
                </div>
                <div className='mt-3'>
                    <FormControl fullWidth>
                        <TextField
                            id="outlined-basic"
                            label="Title"
                            variant="outlined"
                            required
                            value={title}
                            onChange={(e) => {
                                setTitle(e.target.value)
                            }} />
                    </FormControl>
                </div>
                <div className='mt-3'>
                    <label htmlFor="icon-button-file">
                        <Input accept="image/*" id="icon-button-file" type="file" onChange={(e) => setImage(e.target.files[0])} style={{ display: 'none' }} />
                        <IconButton color="primary" aria-label="upload picture" component="span">
                            <PhotoCamera /> Incident Image
                        </IconButton>
                        <InputLabel data-cy="image-name">{image.name || ''}</InputLabel>
                    </label>
                </div>
                <div className='mt-3'>
                    <FormControl fullWidth>
                        <TextField
                            data-cy="lat"
                            id="outlined-basic"
                            label="Latitude"
                            variant="outlined"
                            required
                            disabled
                            value={locations.lat}
                            onChange={(e) => {
                                setTitle(e.target.value)
                            }} />
                        <TextField
                            data-cy="lng"
                            className='mt-3'
                            id="outlined-basic"
                            label="Longitude"
                            variant="outlined"
                            required
                            disabled
                            value={locations.long}
                            onChange={(e) => {
                                setTitle(e.target.value)
                            }} />
                    </FormControl>
                </div>
                <div className='mt-3 d-flex justify-content-center'>
                    <Button variant="contained" data-cy="submit" endIcon={<SendIcon />} onClick={handleSubmit}>
                        Send
                    </Button>
                </div>
            </Box>
        </div>
    )
}

export default IncidentForm