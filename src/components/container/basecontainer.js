import React from 'react';

export default function BaseContainer(props) {
    return (
        <div style={}>
            {props.children}
        </div>
    );
}