import Plot from 'react-plotly.js';

const labels = ['Oxygen','Hydrogen','Carbon_Dioxide','Nitrogen'];
const values = [4500, 2500, 1053, 500];
// const  fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)]);
// fig.show()
const PieChart = (props) => (
  props.data && props.data.zones?
    <div>
        <Plot
          data={[
            {
                labels: Object.keys(JSON.parse(props.data.zones)),
                values: Object.values(JSON.parse(props.data.zones)),
                type: 'pie',
                hole: .4,
                textinfo: "label+percent",
                insidetextorientation: "radial"
            }
          ]}
         layout={ {title: 'Zone wise demands', paper_bgcolor:'rgba(0,0,0,0)', plot_bgcolor:'rgba(0,0,0,0)'} } />
    </div>
  :<></>
)

export default PieChart;