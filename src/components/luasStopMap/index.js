import React, { useState } from 'react'
import ReactMapGL, { Marker } from 'react-map-gl';
import RoomIcon from '@mui/icons-material/Room';


function LuasStopMap({ stop }) {
    const [viewport, setViewport] = useState()
    const INITIAL_VIEW_STATE = {
        latitude: 53.350140,
        longitude: -6.266155,
        zoom: 10,
        pitch: 30,
        bearing: 0
    };

    const TOKEN = 'pk.eyJ1Ijoic2Rhc2giLCJhIjoiY2wxYzZhcGd1MDNydjNndGY5bWg3bG5yMSJ9.3bbjCSSNs9pTNsOFyY1FLQ';
    return (
        <div className = 'map-container' style = {{height: '40vh', margin:'0% 20%'}}>
            <ReactMapGL
                initialViewState={INITIAL_VIEW_STATE}
                onViewportChange={(new_view) => setViewport(new_view)}
                mapStyle="mapbox://styles/mapbox/dark-v10"
                mapboxAccessToken={TOKEN}
                style={{ top: '3%' }}
            >
                {
                    Object.keys(stop).length && <Marker
                        latitude={stop.lat}
                        longitude={stop.lng}>
                        <RoomIcon className='text-danger' />
                        <span className='text-white'>{stop.stop_name}</span>
                    </Marker>
                }
            </ReactMapGL>
        </div>
    )
}

export default LuasStopMap