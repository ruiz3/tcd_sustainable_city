import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import bikePic from '../../styles/img/bike1.png'

export default function MediaCard(props) {
  return (
    <div className='col-sm-3	col-md-3' style={{ marginBottom: 20 }}>
      <Card sx={{ maxWidth: props.width ? props.width : 345, height: props.height ? props.height : 370 }}>
        <CardMedia
          component="img"
          height="140"
          image={props.displayPic ? props.displayPic : bikePic}

        />
        <CardContent sx={{ height: '50%', overflow: 'scroll' }}>
          <Typography gutterBottom variant="h5" component="div">
            {props.heading}
          </Typography>
          <hr />
          <Typography variant="body2" color="text.secondary" data-cy={props.heading}>
            {props.body instanceof Array ? (
              <>
                {props.body.map((el, index) => (
                  <li key={index}>{el}</li>
                ))}
              </>
            ) : (props.body)}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
}