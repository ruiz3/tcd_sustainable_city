import React, { useEffect, useState } from 'react'
import { getLuasDetail } from '../../network/luas'

function LuasTimeTable({ stop, inbound }) {
    const [detail, setDetail] = useState({})
    useEffect(() => {
        console.log(stop)
        getLuasDetail(stop.stop_number).then(res => {
            console.log(res)
            setDetail(res[0])
        })
    }, [stop])
    return (
        <>
            {Object.keys(detail).length ?
                <table className="table table-dark mt-5 w-50" >
                    <thead>
                        <tr>
                            <th scope="col">Destination</th>
                            <th scope="col">Minute</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {inbound ? (<>
                                <td>{detail.inbound.destination}</td>
                                <td>{detail.inbound.due}</td>
                            </>) : (
                                <>
                                    <td>{detail.outbound.destination}</td>
                                    <td>{detail.outbound.due}</td>
                                </>
                            )}

                        </tr>
                        <tr>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                    </tbody>
                </table>
                : <></>
            }
        </>
    )
}

export default LuasTimeTable