import React from 'react'
// ui part
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import TrainIcon from '@mui/icons-material/Train';
import TrafficIcon from '@mui/icons-material/Traffic';
import ElectricBikeIcon from '@mui/icons-material/ElectricBike';
import InsightsIcon from '@mui/icons-material/Insights';
import SubwayIcon from '@mui/icons-material/Subway';
import ReportOffIcon from '@mui/icons-material/ReportOff';
import Drawer from '@mui/material/Drawer';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import LogoutIcon from '@mui/icons-material/Logout';
import DateRangeIcon from '@mui/icons-material/DateRange';
// route
import { Link } from 'react-router-dom'
const SideBar = (props) => {
    const { setOpen, open, drawerWidth, isManager } = props
    const icons = [
        <DateRangeIcon />,
        <SubwayIcon />,
        <TrainIcon />,
        <ElectricBikeIcon />,
        <TrafficIcon />,
        <ReportOffIcon />,
        <InsightsIcon />
    ]
    const routes = isManager ? [
        '/events',
        '/luas',
        '/train',
        '/bike',
        '/harzards',
        '/incident',
        '/analytics'
    ] : [
        '/events',
        '/luas',
        '/train',
        '/bike',
        '/harzards',
        '/incident'
    ]

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const ListItems = isManager ? ['Events', 'Luas', 'Rail', 'Bikes', 'Traffic Information', 'Incident Reports', 'Analytics'] : ['Events', 'Luas', 'Rail', 'Bikes', 'Traffic Information', 'Incident Reports']
    return (
        <>
            <Toolbar className='p-0' style={{ backgroundColor: 'grey' }}>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={handleDrawerOpen}
                    edge="start"
                    sx={{ paddingLeft: '25px', ...(open && { display: 'none' }) }}
                    data-cy="icon"
                >
                    <MenuIcon />
                </IconButton>
            </Toolbar>
            <Drawer
                sx={{
                    width: drawerWidth,
                    flexShrink: 0,
                    '& .MuiDrawer-paper': {
                        width: drawerWidth,
                        boxSizing: 'border-box',
                    }
                }}
                variant="persistent"
                anchor="left"
                open={open}
            >
                <div className='d-flex justify-content-between align-items-center text-white' style={{ backgroundColor: 'grey' }} data-cy="sideHeader">
                    Menu
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List className="min-vh-100" style={{ backgroundColor: 'grey' }}>
                    {ListItems.map((text, index) => (
                        <Link to={routes[index]} style={{ textDecoration: 'none', color: 'black' }} data-cy={text.split(' ')[0]}>
                            <ListItem button key={text} className='mt-3'>
                                <ListItemIcon className='text-white'>
                                    {icons[index]}
                                </ListItemIcon>
                                <ListItemText primary={text} className="text-white" />
                            </ListItem>
                        </Link>
                    ))}
                    <ListItem button key='Sign Out' className='mt-3' onClick={props.signOff}>
                        <ListItemIcon className='text-white'>
                            <LogoutIcon />
                        </ListItemIcon>
                        <ListItemText primary='Sign Out!' className='text-white' />
                    </ListItem>
                </List>
            </Drawer>
        </>
    )
}

export default SideBar