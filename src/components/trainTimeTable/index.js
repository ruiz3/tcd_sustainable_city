import React from 'react'

function TrainTimeTable({ rails }) {
    return (
        <table className='table table-bordered table-striped'>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Rail Code</th>
                    <th scope="col">Departure</th>
                    <th scope="col">Arrival</th>
                    <th scope='col'>Time</th>
                    <th scope='col'>Message</th>
                </tr>
            </thead>
            <tbody>
                {rails.map((rail, index) => (
                    <tr>
                        <th scope="row">{index}</th>
                        <td>{rail.train_code}</td>
                        <td>{rail.originating_station}</td>
                        <td>{rail.destination_station}</td>
                        <td>{rail.departure_time}</td>
                        <td>{rail.Message ? rail.Message : 'On-Schedule'}</td>
                    </tr>
                ))}

            </tbody>
        </table>
    )
}

export default TrainTimeTable