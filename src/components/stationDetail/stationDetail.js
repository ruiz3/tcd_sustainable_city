import React, { useEffect, useState } from 'react';
import Plot from 'react-plotly.js';
import { range } from '../../utils/range';
let stations = JSON.parse(localStorage.getItem('avail_bikes'))
export default function StationDetail({ keyword, predictions }) {
    const [current, setCurrent] = useState([])
    const [future, setFuture] = useState([])
    useEffect(() => {
        console.log(stations)
        const res = stations.filter(station => {
            // return station[0] ? station[0].name === keyword : false
            return station.name === keyword
        })
        console.log('prediction_data', predictions)
        const prediction = Object.keys(predictions).length ? predictions[keyword] : []
        console.log('prediction', prediction)
        console.log(res)
        if (res.length) {
            setCurrent(res[0].num)
        }
        console.log(current)
        setFuture(prediction)
    }, [keyword, stations, predictions])
    useEffect(() => {
        stations = JSON.parse(localStorage.getItem('avail_bikes'))
        const res = stations.filter(station => {
            return station.name === keyword
        })
        if (res.length) {
            setCurrent(res[0].num)
        }
        console.log(current)
    }, [localStorage.getItem('avail_bikes')])
    return (
        <div>
            {
                (current.length && future.length) ? (
                    <>
                        <Plot
                            data={[
                                {
                                    x: range(current),
                                    y: current,
                                    type: 'scatter',
                                    mode: 'lines+markers',
                                    marker: { color: 'red' },
                                    name: "current"
                                },
                                {
                                    x: range(future),
                                    y: future,
                                    line: {
                                        dash: 'dot',
                                        width: 2
                                    },
                                    marker: { color: 'orange' },
                                    name: 'prediction'
                                }
                            ]}
                            layout={{ title: 'Bike station Plot' }}
                            style={{ width: "100%", height: 800, padding: 0 }}
                        />
                    </>
                ) : (
                    <></>
                )
            }
        </div>
    );
}
