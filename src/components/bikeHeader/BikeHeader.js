import React from 'react';
import MediaCard from '../card/Card';

export default function BikeHeader({statistics}) {
    const titles = ['Total Stations', 'Total Bikes', 'Empty Stations', 'Available Stations']
    const data = [statistics.total_bike_stands, statistics.total_bikes, statistics.empty_stations, statistics.avail_stations]
    return (
        <div className='row' style={{ width: '95%' }}>
            {titles.map((el, index) =>
                (<MediaCard key={index} heading={titles[index]} body={data[index]} />)
            )}
        </div>
    )
}
