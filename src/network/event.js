import request from './request'

export const getEvents = () => {
    return request(
        '/api/',
        {
            url: 'get/events/'
        }
    )
}