import request from './request'

export function getBikeData() {
    return request(
        '/api/',
        {
            url: 'get/dublinbikes/'
        }
    )
}