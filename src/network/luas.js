import request from './request'

export function getLuasStop() {
    return request(
        '/api/',
        {
            url: 'get/luasstops/'
        }
    )
}

export function getLuasDetail(id) {
    return request(
        '/api/',
        {
            url: 'get/luasstation/',
            params: {
                id
            }
        }
    )
}

export function getLuasLineStatus() {
    return request(
        '/api/',
        {
            url: 'get/luasline/'
        }
    )
}