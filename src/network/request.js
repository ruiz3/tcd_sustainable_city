import axios from 'axios'

export default function getData(url, option) {
    return new Promise((resolve, reject) => {
        const instance = axios.create({
            baseURL: url,
            timeout: 5000
        })

        instance.interceptors.request.use(config => {
            return config
        }, err => {
            return err
        })

        instance.interceptors.response.use(response => {
            return response.data
        }, err => {
            console.log(err);
        })

        // 2.传入对象进行网络请求
        if (option.method === 'POST') {
            instance.post(option.url, option.data).then(res => {
                resolve(res)
            }).catch(err => {
                reject(err)
            })
        } else {
            if (option.params) {
                instance(option.url, {
                    params: option.params
                }).then(res => {
                    resolve(res)
                }).catch(err => {
                    reject(err)
                })
            } else {
                instance(option).then(res => {
                    resolve(res)
                }).catch(err => {
                    reject(err)
                })
            }
        }
    })
}