import request from './request'

export function getAnalyticsData() {
    return request(
        '/api/',
        {
            url: 'get/ml_predictions/'
        }
    )
}