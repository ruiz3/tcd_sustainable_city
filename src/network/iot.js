import Amplify, { Auth } from 'aws-amplify';
import { AWSIoTProvider } from '@aws-amplify/pubsub/lib/Providers';
import { get_live_bike_action, get_prediction_bike_data } from '../store/bike/actionCreators'
export function registerToTopic() {
    console.log('Register')
    Auth.currentAuthenticatedUser().then((info) => {
        console.log('Auth.currentAuthenticatedUser')
        console.log(info.username)

        //To trigger mail with code to the registered user:
        // Auth.forgotPassword(info.username)
        // .then(data=>{
        //   console.log(data)
        // })

        //To submit code sent to mail with new_password;
        // Auth.forgotPasswordSubmit(username, code, new_password)
        // .then(data => console.log(data))
        // .catch(err => console.log(err));
    })
    Auth.currentCredentials().then((info) => {
        const cognitoIdentityId = info.identityId;
        console.log(info)
    });


    //IOTPlug
    Amplify.addPluggable(new AWSIoTProvider({
        aws_pubsub_region: 'eu-west-1',
        aws_pubsub_endpoint: 'wss://a2ae8fkmdhpg75-ats.iot.eu-west-1.amazonaws.com/mqtt',
    }));
}

export function subscribeToTopic(name, dispatch) {
    Amplify.PubSub.subscribe(name).subscribe({
        next: data => {
            console.log(data)
            if (data) {
                if (data.value.predictions.length) {
                    dispatch(get_prediction_bike_data(JSON.parse(data.value.predictions)))
                } else if (data.value.live.length) {
                    dispatch(get_live_bike_action(data.value))
                }
            }
        },
        error: error => console.error('Error has occurred:', error),
        close: () => console.log('Done'),
    });
}