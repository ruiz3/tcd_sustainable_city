import request from './request'

export function getIrishRail() {
    return request(
        '/api/',
        {
            url: 'get/irishrail/'
        }
    )
}