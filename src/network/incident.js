import request from './request'

export function postIncident(data, headers) {
    return request(
        '/api/',
        {
            method: 'POST',
            url: 'incidentform/',
            headers,
            data
        }
    )
}