import request from './request'

export function getHazardsData() {
    return request(
        '/api/',
        {
            url: 'get/incidents/'
        }
    )
}