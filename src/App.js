import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import SideBar from './components/sidebar/siderbar';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import BikePage from './pages/BikePage'
import EventsPage from './pages/EventsPage'
import HarzardsPage from './pages/HarzardsPage';
import LuasPage from './pages/LuasPage';
import TrainPage from './pages/TrainPage';
import IncidentPage from './pages/IncidentPage';
import AnalyticsPage from './pages/AnalyticsPage'
import HomePage from './pages/HomePage';
import { Provider } from 'react-redux'
import store from './store'
import Amplify from 'aws-amplify';
import { useAuthenticator, Authenticator, TextAreaField, RadioGroupField, Radio, Heading } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import { awsCognitoConfig } from './network/aws-cognito-config';
import loginBackground from './styles/img/background.jpeg';
import { registerToTopic } from './network/iot';
import './styles/css/app.css'

function handleAuthStateChange() {
  return
}

const drawerWidth = 240
const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

function App() {
  const [open, setOpen] = React.useState(false);
  const [isManager, setIsManager] = React.useState(false);
  React.useEffect(() => {
    Amplify.configure({
      Auth: {
        mandatorySignId: true,
        identityPoolId: awsCognitoConfig.IDENTITY_POOL_ID,
        region: awsCognitoConfig.REGION,
        userPoolId: awsCognitoConfig.USER_POOL_ID,
        userPoolWebClientId: awsCognitoConfig.APP_CLIENT_ID
      }
    });
    registerToTopic()
  }, [])

  return (
    <div className='container-class' style={{ backgroundImage: `url(${loginBackground})`, backgroundSize: 'cover'/*,paddingTop:'28vh',paddingBottom:'28vh'*/ ,minHeight:'100vh'}}>

      <Authenticator hideDefault={true} loginMechanisms={['email']} authState="noState" onStateChange={handleAuthStateChange} signUpAttributes={[
        'address',
        'birthdate',
        'email',
        'family_name',
        'gender',
        'name',
        'phone_number',
        'preferred_username'
      ]}
        components={
          {
            Header() {
              return (
                <h3 style={{ fontFamily: 'fantasy', textAlign: 'center', backgroundColor:'#047d95',padding:'10px 0px', borderBottom:'5px black solid' ,color:'white'}}> {/*Hiragino Sans GB*/}
                  Sustainable City Management
                </h3>
              )
            },
            SignUp: {
              FormFields() {
                const { validationErrors } = useAuthenticator();

                return (
                  <>
                    {/* Re-use default `Authenticator.SignUp.FormFields` */}
                    <Authenticator.SignUp.FormFields />

                    {/* Append & require Terms & Conditions field to sign up  */}
                    <TextAreaField
                      errorMessage={validationErrors.acknowledgement}
                      hasError={!!validationErrors.acknowledgement}
                      name="address"
                      label="Address"
                    />
                    <RadioGroupField label="Gender" name="gender">
                      <Radio value="Male">Male</Radio><Radio value="Female">Female</Radio>
                      <Radio value="Other">Other</Radio>
                    </RadioGroupField>
                  </>
                );
              },
            },
          }}>
        {({ signOut, user }) => {
          var user_groups = user.signInUserSession.accessToken.payload["cognito:groups"] ? user.signInUserSession.accessToken.payload["cognito:groups"] : false
          if (user_groups)
            user_groups.indexOf('City_Manager') >= 0 ? setIsManager(true) : setIsManager(false)
          else
            setIsManager(false)
          return (
            <Provider store={store}>
              <BrowserRouter>
                <Box sx={{ display: 'flex', alignItems: 'start' }}>
                  <SideBar setOpen={setOpen} open={open} drawerWidth={drawerWidth} isManager={isManager} signOff={signOut} />
                  <Main open={open} style={{ padding: '0px' }}>
                    <Routes>
                      <Route path="/train" element={<TrainPage />} />
                      <Route path="/luas" element={<LuasPage />} />
                      <Route path="/bike" element={<BikePage />} />
                      <Route path="/events" element={<EventsPage />} />
                      <Route path="/harzards" element={<HarzardsPage />} />
                      <Route path="/incident" element={<IncidentPage user={user} />} />
                      {isManager ? <Route path="/analytics" element={<AnalyticsPage />} /> : <></>}
                      <Route path="/" element={<HomePage />} />
                    </Routes>
                  </Main>
                </Box>
              </BrowserRouter>
            </Provider>
          )
        }}
      </Authenticator>
    </div>
  );
}

export default App;