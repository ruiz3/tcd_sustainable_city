const live_data = {
    "live": [
        {
            "total_bike_stands": 2103,
            "total_bikes": 1417,
            "harvest_time": "2022-03-23T22:30:03",
            "empty_stations": [
                "Custom House Quay",
                "Exchequer Street",
                "Newman House",
                "St James Hospital (Luas)",
                "Fitzwilliam Square East",
                "Benson Street",
                "Heuston Station (Car Park)",
                "Grangegorman Lower (South)"
            ],
            "avail_stations": [
                "Charlemont Street",
                "Ormond Quay Upper",
                "Talbot Street",
                "Portobello Road",
                "Deverell Place",
                "Excise Walk",
                "The Point",
                "Emmet Road",
                "Brookfield Road",
                "South Dock Road",
                "Heuston Bridge (North)",
                "Heuston Station (Central)",
                "Royal Hospital",
                "Kilmainham Gaol",
                "Frederick Street South",
                "Avondale Road",
                "Phibsborough Road",
                "Killarney Street"
            ]
        }
    ],
    "predictions": []
}

const prediction_data = {
    "predictions": { 'BLESSINGTON STREET': [10.0, 11.0, 12.0], 'BOLTON STREET': [1.0], 'GREEK STREET': [11.0], 'CHARLEMONT PLACE': [25.0], 'CHRISTCHURCH PLACE': [6.0] },
    "live": [],
    "topic": "feed"
}

export {
    live_data,
    prediction_data
}

