describe('Login', () => {
    before(() => {
        cy.visit('/', {
            onBeforeLoad(win) {
                // e.g., force Barcelona geolocation
                const latitude = 53.3416238;
                const longitude = -6.2533667;
                cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake((cb) => {
                    return cb({ coords: { latitude, longitude } });
                });
            },
        })
    })

    describe('Wrong email format', () => {
        beforeEach(() => {
            cy.get('#amplify-id-0').clear()
            cy.get('#amplify-id-1').clear()
        })
        it('without @', () => {
            cy.get('#amplify-id-0').type('123')
            cy.get('#amplify-id-1').type('123')
            cy.get("button[type='submit']").click()
            cy.get("button[type='submit']").should('contain', 'Sign in')
        })

        it('without content after @', () => {
            cy.get('#amplify-id-0').type('123@')
            cy.get('#amplify-id-1').type('123')
            cy.get("button[type='submit']").click()
            cy.get("button[type='submit']").should('contain', 'Sign in')
        })
    })

    describe('Wrong username or password', () => {
        beforeEach(() => {
            cy.get('#amplify-id-0').clear()
            cy.get('#amplify-id-1').clear()
        })
        it('wrong username', () => {
            cy.get('#amplify-id-0').type('swagatdash99@gmail.com')
            cy.get('#amplify-id-1').type('Qwerty123#')
            cy.get("button[type='submit']").click()
            cy.get("button[type='submit']").should('contain', 'Sign in')
        })

        it('wrong password', () => {
            cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
            cy.get('#amplify-id-1').type('Qwerty12')
            cy.get("button[type='submit']").click()
            cy.get("button[type='submit']").should('contain', 'Sign in')
        })
    })

    describe('Authenticated and turn to home page', () => {
        beforeEach(() => {
            cy.get('#amplify-id-0').clear()
            cy.get('#amplify-id-1').clear()
        })
        it('Login success', () => {
            cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
            cy.get('#amplify-id-1').type('Qwerty123#')
            cy.get("button[type='submit']").click()
            cy.wait(3000)
            cy.get('[data-cy=Home]').should('contain', 'Home')
        })

    })
})