describe('Event Page', () => {
    before(() => {
        cy.visit('/', {
            onBeforeLoad(win) {
                // e.g., force Barcelona geolocation
                const latitude = 53.3416238;
                const longitude = -6.2533667;
                cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake((cb) => {
                    return cb({ coords: { latitude, longitude } });
                });
            },
        })
        cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
        cy.get('#amplify-id-1').type('Qwerty123#')
        cy.get("button[type='submit']").click()
        cy.wait(3000)
        cy.get("[data-cy=icon]").click()
        cy.get("[data-cy=Events]").click()
    })

    describe('Render data', () => {
        it('should render the calendar', () => {
            cy.get('[data-cy=calendar]').should('not.to.match', ':empty')
        })
    })
})