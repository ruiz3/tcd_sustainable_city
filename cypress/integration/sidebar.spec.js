describe('Side bar test', () => {
    before(() => {
        cy.visit('/', {
            onBeforeLoad(win) {
                // e.g., force Barcelona geolocation
                const latitude = 53.3416238;
                const longitude = -6.2533667;
                cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake((cb) => {
                    return cb({ coords: { latitude, longitude } });
                });
            },
        })
        cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
        cy.get('#amplify-id-1').type('Qwerty123#')
        cy.get("button[type='submit']").click()
        cy.wait(3000)
        cy.get("[data-cy=icon]").click()
    })
    describe('Route test', () => {
        it('displays the Rail url', () => {
            cy.get("[data-cy=Rail]").click()
            cy.url().should('include', 'train')
        })
        it('displays the Luas url', () => {
            cy.get("[data-cy=Luas]").click()
            cy.url().should('include', 'luas')
        })
        it('displays the Train url', () => {
            cy.get("[data-cy=Rail]").click()
            cy.url().should('include', 'train')
        })
        it('displays the Bike url', () => {
            cy.get("[data-cy=Bikes]").click()
            cy.url().should('include', 'bike')
        })
        it('displays the Incident url', () => {
            cy.get("[data-cy=Incident]").click()
            cy.url().should('include', 'incident')
        })
    })

})