describe('Bike page test', () => {
    before(() => {
        cy.visit('/', {
            onBeforeLoad(win) {
                // e.g., force Barcelona geolocation
                const latitude = 53.3416238;
                const longitude = -6.2533667;
                cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake((cb) => {
                    return cb({ coords: { latitude, longitude } });
                });
            },
        })
        cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
        cy.get('#amplify-id-1').type('Qwerty123#')
        cy.get("button[type='submit']").click()
        cy.wait(3000)
        cy.get("[data-cy=icon]").click()
        cy.get("[data-cy=Rail]").click()
        cy.wait(3000)
    })

    describe('render data test', () => {
        it('should render data correctly', () => {
            cy.get("table").should('not.to.match', ':empty')
        })

        it('should change location after clicking select button', () => {
            cy.get('[data-cy=select]').click()
            cy.get('li[data-value=0]').click()
            cy.get('tr td:nth-child(3)').each(($item) => {
                expect($item).to.contain('Cork')
            })
        })
    })
})