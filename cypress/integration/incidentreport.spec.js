const fixtureFile = 'img/test.jpeg';
describe('Incident Report test', () => {
    before(() => {
        cy.visit('/', {
            onBeforeLoad(win) {
                // e.g., force Barcelona geolocation
                const latitude = 53.3416238;
                const longitude = -6.2533667;
                cy.stub(win.navigator.geolocation, 'getCurrentPosition').callsFake((cb) => {
                    return cb({ coords: { latitude, longitude } });
                });
            },
        })
        cy.get('#amplify-id-0').type('swagatdash95@gmail.com')
        cy.get('#amplify-id-1').type('Qwerty123#')
        cy.get("button[type='submit']").click()
        cy.wait(3000)
        cy.get("[data-cy=icon]").click()
    })

    describe('incident report', () => {
        before(() => {
            cy.get("[data-cy=Incident]").click()
        })

        it('latitude and longitude should be disabled', () => {
            cy.get("[data-cy=lat] input").should('be.disabled')
            cy.get("[data-cy=lng] input").should('be.disabled')
        })

        it('should show img name after uploading', () => {
            cy.get('input[type=file]').attachFile(fixtureFile);
            cy.get('[data-cy=image-name]').should('contain', 'test')
        })

        it('should show success after uploading', () => {
            cy.get('[data-cy=email] input').type('ruiz@tcd.ie')
            cy.get('[data-cy=submit]').click()
            cy.on('window:alert', (str) => {
                expect(str).to.equal('Upload Success')
            })
        })
    })
})